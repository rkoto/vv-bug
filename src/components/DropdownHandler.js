import BodyFreezerMixin from '@/components/BodyFreezerMixin'

/**
 * A mixin that handles dropdown visibility changes
 * Requires BodyFreezer mixin and updateAvailableValues that will perform actions when a dropdown is closed
 */
export default {
  mixins: [
    BodyFreezerMixin
  ],
  data () {
    return {
      // Individual mixins add a proper field to this object. That way it knows what to hide when other dropdown is being opened.
      dropdowns: {}
    }
  },
  methods: {
    toggle (dropdown, id) {
      this.hideAllDropdownsExcluding(dropdown, id)
      this.toggleDropdownVisibility(dropdown, id)
    },
    hideAllDropdownsExcluding (name) {
      if (typeof !this.dropdowns[name] === 'object') {
        for (const dropdown in this.dropdowns) {
          if (dropdown !== name) this.dropdowns[dropdown] = false
        }
      }
    },
    toggleDropdownGroupChild (dropdown, id) {
      this.$set(this.dropdowns[dropdown], id, !this.dropdowns[dropdown][id])
    },
    toggleDropdownVisibility (dropdown, id) {
      if (typeof this.dropdowns[dropdown] === 'object') this.toggleDropdownGroupChild(dropdown, id)
      else this.dropdowns[dropdown] = !this.dropdowns[dropdown]
    },
    isAnyDropdownVisible () {
      for (const dropdown in this.dropdowns) {
        if (this.dropdowns[dropdown]) return true
      }
      return false
    }
  }
}
