function isFieldValid (flags) {
  if (flags) {
    console.log('flags.validated', flags.validated)
    console.log('flags.valid', flags.valid)
    console.log('flags.changed', flags.changed)
    console.log('___________________________')
  }
  return flags && flags.validated && flags.valid && flags.changed
}

function isFieldInvalid (flags) {
  return flags && flags.validated && flags.invalid
}

export default {
  inject: [ '$validator' ],

  props: {
    name: {
      type: String,
      required: false
    }
  },

  methods: {
    getValidationClasses (...additionalClasses) {
      return additionalClasses.reduce((acc, cls) => Object.assign({ [cls]: true }, acc), {
        'field-valid': isFieldValid(this.fields[this.name]),
        'field-invalid': isFieldInvalid(this.fields[this.name])
      })
    },

    /**
     * There we need to have it standarized that if we have multiple fields of one type
     * i.e. "firstName" then it must follow that rule -> firstName_123
     *
     * @param {String} validateAs
     */
    validationField (validateAs) {
      return validateAs && validateAs.indexOf('_') > -1 ? this.extendLocalPlDictionary(validateAs) : validateAs
    },

    /**
     * Extends main dictionary object (already only PL) with additional items.
     *
     * This solution is needed because of lack of library source solution (data-scope not works over iterated elements).
     * If we have situation when we iterate over elements that needs to be validated and if
     * these items have the samve validation we just need to add main field with validation messages
     * and there it will be cloned to new fields with matching names.
     *
     * @param {String} dictionaryItem
     */
    extendLocalPlDictionary (dictionaryItem) {
      const original = dictionaryItem.split('_')[0]
      this.$validator.dictionary.container.pl.custom = {
        ...this.$validator.dictionary.container.pl.custom,
        [dictionaryItem]: this.$validator.dictionary.container.pl.custom[original]
      }

      return dictionaryItem
    }
  }
}
