import './BodyFreezerMixin.scss'

function isFluidLayoutPage () {
  return window.innerWidth < 768
}

/**
 * A mixin that adds document.body freezing capabilities
 */
export default {
  methods: {
    setBodyHeight (element) {
      const vpH = window.innerHeight
      element.style.height = `${vpH}px`
    },
    resetBodyHeight (element) {
      element.style.height = 'auto'
    },
    freezeBody () {
      if (isFluidLayoutPage()) document.documentElement.classList.add('body-freeze')
      document.body.classList.add('body-freeze')

      this.setBodyHeight(document.body)
    },
    unfreezBody () {
      if (isFluidLayoutPage()) document.documentElement.classList.remove('body-freeze')
      document.body.classList.remove('body-freeze')

      this.resetBodyHeight(document.body)
    }
  }
}
