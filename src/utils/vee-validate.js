import Vue from 'vue'
import VeeValidate, { Validator } from 'vee-validate'

import customMessagesPl from '@/utils/validation-dictionary/locals_PL'

Validator.extend('moment_not_zero', {
  getMessage: field => `Pole ${field} jest wymgane`,
  validate: value => !!value.isZero && !value.isZero()
})

Vue.use(VeeValidate, {
  classes: true,
  classNames: {
    valid: 'field-valid',
    invalid: 'field-invalid'
  },
  events: 'change',
  dictionary: {
    pl: { custom: customMessagesPl }
  }
})

Validator.localize('pl')
