export default class RandomIdGenerator {
  static newId () {
    return `rndid-${(Math.random() * 1000000).toString(32)}`
  }
}
