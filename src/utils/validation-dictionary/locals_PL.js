import moment from 'moment'

/* eslint-disable */
export default {
  firstName: {
    alpha_spaces: () => 'Podaj imię zawierające min. 2 znaki. Dopuszczalne są wielkie i małe litery, spacja, myślnik',
    required: () => 'Podaj imię zawierające min. 2 znaki. Dopuszczalne są wielkie i małe litery, spacja, myślnik',
    min: () => 'Podaj imię zawierające min. 2 znaki. Dopuszczalne są wielkie i małe litery, spacja, myślnik',
    regex: () => 'Podaj imię zawierające min. 2 znaki. Dopuszczalne są wielkie i małe litery, spacja, myślnik',
  },
  lastName: {
    regex: () => 'Podaj nazwisko zawierające min. 2 znaki. Dopuszczalne są wielkie i małe litery, spacja, myślnik',
    required: () => 'Podaj nazwisko zawierające min. 2 znaki. Dopuszczalne są wielkie i małe litery, spacja, myślnik',
  },
  email: {
    email: () => 'Podaj poprawny adres e-mail z uwzględnieniem “@”, np. jan.kowalski@przyklad.pl . Adres nie może zawierać polskich znaków ani znaków specjalnych, z wyjątkiem myślników, podkreśleń, kropek i cyfr.',
    required: () => 'Podaj poprawny adres e-mail z uwzględnieniem “@”, np. jan.kowalski@przyklad.pl . Adres nie może zawierać polskich znaków ani znaków specjalnych, z wyjątkiem myślników, podkreśleń, kropek i cyfr.',
  },
  phoneNumber: {
    numeric: () => 'Wpisz poprawny numer telefonu zawierający minimum 7 cyfr.',
    min: () => 'Wpisz poprawny numer telefonu zawierający minimum 7 cyfr.',
    required: () => 'Wpisz poprawny numer telefonu zawierający minimum 7 cyfr.',
  },
  street: {
    regex: () => 'Wpisz poprawną ulicę, osiedle lub aleję np. Al. Piastów',
    required: () => 'Wpisz poprawną ulicę, osiedle lub aleję np. Al. Piastów',
  },
  houseNumber: {
    regex: () => 'Wpisz poprawny numer domu',
    required: () => 'Wpisz poprawny numer domu',    
  },
  apartmentNumber: {
    regex: () => 'Wpisz poprawny numer mieszkania',
    required: () => 'Wpisz poprawny numer mieszkania',
  },
  city: {
    regex: () => 'Wpisz poprawną nazwę miejscowości',
    required: () => 'Wpisz poprawną nazwę miejscowości',
  },
  zipCodeFirstPart: {
    numeric: () => 'Wpisz poprawny kod pocztowy w formacie XX-XXX',
    min: () => 'Wpisz poprawny kod pocztowy w formacie XX-XXX',
    required: () => 'Wpisz poprawny kod pocztowy w formacie XX-XXX',
  },
  zipCodeSecondPart: {
    numeric: () => 'Wpisz poprawny kod pocztowy w formacie XX-XXX',
    min: () => 'Wpisz poprawny kod pocztowy w formacie XX-XXX',
    required: () => 'Wpisz poprawny kod pocztowy w formacie XX-XXX',
  },
  gender: {
    required: () => 'Pole płeć jest wymagane',
  },
  birthday: {
    moment_not_zero: () => 'Podaj poprawną datę urodzenia w formacie DD-MM-RRRR',
    required: () => 'Podaj poprawną datę urodzenia w formacie DD-MM-RRRR',
    before: (fieldName, params) => `Wybierz datę wcześniejszą od ${moment(params[0]).format('L')}`,
    after: (fieldName, params) => `Wybierz datę późniejszą od ${moment(params[0]).format('L')}`,
  },
}
/* eslint-enable */
