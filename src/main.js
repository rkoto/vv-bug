import Vue from 'vue'
import App from './App.vue'
import store from './store'

import './utils/vee-validate'

const log = window.log = require('loglevel')
log.setDefaultLevel('trace')
log.info('Logging enabled')

Vue.config.productionTip = false

new Vue({
  store,
  render: h => h(App)
}).$mount('#app')
